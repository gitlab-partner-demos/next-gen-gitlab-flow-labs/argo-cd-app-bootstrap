#! /bin/bash

gcloud auth activate-service-account $SERVICE_ACCOUNT_NAME --key-file=$KEYFILE_PATH_JSON
gcloud config set project $PROJECT_NAME
gcloud container clusters get-credentials $GOOGLE_CLOUD_CLUSTER_NAME --region=$GOOGLE_CLOUD_REGION

